require("@nomiclabs/hardhat-waffle");
require("@nomiclabs/hardhat-etherscan");
require('dotenv').config( { path: `.env.${process.env.NODE_ENV}` } )

module.exports = {
  solidity: {
    version: "0.8.7",
    settings: {
      optimizer: {
        enabled: true,
        runs: 500
      }
    }
  },
  paths: {
    sources: "./contracts",
    tests: "./test",
    cache: "./cache",
    artifacts: "./artifacts"
  },
  networks: {
    hardhat: { gas: 15000000 },
    rinkeby: {
      url: 'https://rinkeby.infura.io/v3/' + process.env.INFURA_PROJECT_ID,
      chainId: 4,
      gas: 15000000,
      accounts: (process.env.PRIVATE_KEY || '').split(' '),
    },
    goerli: {
      url: 'https://goerli.infura.io/v3/' + process.env.INFURA_PROJECT_ID,
      chainId: 5,
      gas: 15000000,
      accounts: (process.env.PRIVATE_KEY || '').split(' '),
    },
    ropsten: {
      url: 'https://ropsten.infura.io/v3/' + process.env.INFURA_PROJECT_ID,
      chainId: 3,
      gas: 15000000,
      accounts: (process.env.PRIVATE_KEY || '').split(' ')
    },
    mumbai: {
      url: 'https://matic-mumbai.chainstacklabs.com',
      chainId: 80001,
      gas: 15000000,
      accounts: (process.env.PRIVATE_KEY || '').split(' ')
    },
    mainnet: {
      url: 'https://mainnet.infura.io/v3/' + process.env.INFURA_PROJECT_ID,
      chainId: 1,
      gas: 15000000,
      ledger: {
          bip32Path: "44'/60'/0'/0/0"
      }
      //accounts: (process.env.PRIVATE_KEY || '').split(' ')
    }
  },
  etherscan: {
    // Your API key for Etherscan
    // Obtain one at https://etherscan.io/
    apiKey: process.env.ETHERSCAN_API_KEY
 }
};

task("balance", "Prints an account's balance")
  .setAction(async (taskArgs) => {
     
  });
