const { network } = require("hardhat")

const {
  deployContract,
  confirmDeployment,
} = require('./deployments')

const _confirmDeployment = contract => confirmDeployment(contract, network.name === 'hardhat' ? 0 : 2)

async function main() {
  return deployContract("OnChainMetadataNFT")
    .then(_confirmDeployment)
}

main()
  .then(() => process.exit(0))
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
