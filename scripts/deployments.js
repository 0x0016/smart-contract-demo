const { formatEther } = require("@ethersproject/units")
const { ethers, network } = require("hardhat")
const Eth = require('@ledgerhq/hw-app-eth').default

/** @typedef {import("ethers").Contract} Contract */
/** @typedef {import("@ethersproject/abstract-provider").TransactionRequest} TransactionRequest */

/**
 * 
 * @param {string} artifactName 
 * @param {?array} constructorArgs 
 * @returns {Promise<Contract>}
 */
async function _deployContract(artifactName, constructorArgs = []) {
  const { address, balance, deploy } = await useHardhat()
  console.info(`\n-----\nDeploying contract ${artifactName} with account ${address} (balance = ${formatEther(balance)} ETH)`)
  console.info(`\nConstructor args:`, constructorArgs)
  return await deploy(artifactName, constructorArgs)
}

async function useHardhat() {
  const [ deployer ] = await ethers.getSigners()
  const { address } = deployer
  const balance = await deployer.getBalance()

  async function deploy(artifactName, constructorArgs = []) {
    const factory = await ethers.getContractFactory(artifactName)
    return factory.deploy(...constructorArgs)
  }

  return { address, balance, deploy }
}


/**
 * 
 * @param {string} tokenBaseURI 
 * @returns {Promise<Contract>}
 */
async function deployContract(name) {
  return await _deployContract(name)
}

/**
 * 
 * @param {Contract} contract 
 * @param {?number} confirmations
 * @return {Promise<Contract>}
 */
async function confirmDeployment(contract, confirmations = 2) {
  console.info(`Waiting for ${confirmations} confirmations...`)
  const tx = contract.deployTransaction
  const receipt = await tx.wait(confirmations)

  const { chainId, hash, gasLimit } = tx
  const { gasUsed } = receipt
  const gasUsedRatio = (gasUsed.toNumber() / gasLimit.toNumber() * 100).toFixed(1)

  console.info(`\nContract deployed successfully!\n`)
  console.info(`Address     ${contract.address}`)
  console.info(`Chain ID    ${chainId}`)
  console.info(`Tx Hash     ${hash}`)
  console.info(`Gas limit   ${gasLimit}`)
  console.info(`Gas used    ${gasUsed} (${gasUsedRatio} %)`)

  return contract
}

module.exports = {
  deployContract,
  confirmDeployment,
}
